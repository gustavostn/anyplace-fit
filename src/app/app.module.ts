import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { BannerComponent } from './banner/banner.component';
import { MainContentComponent } from './main-content/main-content.component';
import { SharedModule } from './shared/shared.module';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PersonalModalComponent } from './home/personal-modal/personal-modal.component';
import { PersonalsContentComponent } from './home/personals-content/personals-content.component';
import { CardsPersonalComponent } from './home/cards-personal/cards-personal.component';


@NgModule({
  declarations: [
    AppComponent,
    InitialPageComponent,
    BannerComponent,
    MainContentComponent,
    LoginComponent,
    PersonalModalComponent,
    PersonalsContentComponent,
    CardsPersonalComponent

  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
