import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'anyplace-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  
  bannerImg:string = '../../assets/Images/banner_homePage.png'
  constructor() { }

  ngOnInit() {
  }

}
