import { Component, OnInit } from '@angular/core';
import { ServicesAnyplaceService } from 'src/app/services-anyplace.service';

@Component({
  selector: 'anyplace-personals-content',
  templateUrl: './personals-content.component.html',
  styleUrls: ['./personals-content.component.scss']
})
export class PersonalsContentComponent implements OnInit {

   public userInfs = null
   public welcomeMsg: string = ""
  // public filterBy:any = null
  // public open: boolean = false
  // public placeholderDropDown: string = "Filtrar por modalidade"
  // public modalidades = [ "Mostrar tudo", "Musculação", "Natação", "Karate", "Muay Thai", "CrossFit", "Pilates", "Atletismo", "Funcional", "Judo", "Aero Dance", "Ginástica Rítma", "Pedalada"]
  
  constructor(private service: ServicesAnyplaceService) { }

  ngOnInit() {
    let data: any = localStorage.getItem("data-user")
    this.userInfs = JSON.parse(data)
    this.getDataUser(this.userInfs)
  }

  getDataUser(infs: any) {
    this.welcomeMsg = ` Olá, ${atob(infs[0].value)}!`
  }

}
