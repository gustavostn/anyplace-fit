import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'cards-personal',
  templateUrl: './cards-personal.component.html',
  styleUrls: ['./cards-personal.component.scss']
})
export class CardsPersonalComponent implements OnInit {

  public hasPersonal: boolean = false
  public handlerPersonal: any = null
  public name: string = ""

  public dataPersonals = [
    { id: 1, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "isabella.wisozk13@gmail.com", number: "+55 (44) 99859-9753", sexo: "Feminino", photo: '../../../assets/Images/prof_isabella.png', name: "Isabella Oliveira", area: "Musculação", location: "São Paulo", },
    { id: 2, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "lucas.wyman@hotmail.com", number: "+55 (54) 92240-8032", sexo: "Masculino", photo: '../../../assets/Images/prof_lucas.png', name: "Lucas Beppu", area: "Natação", location: "São Migual Paulista", },
    { id: 3, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "fernanda_alves94@yahoo.com", number: "+55 (77) 91585-1863", sexo: "Feminino", photo: '../../../assets/Images/prof_fernanda.png', name: "Fernanda Alves", area: "Karate", location: "Mogi das Cruzes" },
    { id: 4, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "anthony44@hotmail.com", number: "+55 (21) 93653-4422", sexo: "Masculino", photo: '../../../assets/Images/prof_anthony.png', name: "Anthony Oliveira", area: "Muay Thai", location: "Pompeia" },
    { id: 5, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "hermes93@gmail.com", number: "+55 (83) 94593-1870", sexo: "Masculino", photo: '../../../assets/Images/prof_hermes.png', name: "Hermes Junior", area: "CrossFit", location: "Suzano" },
    { id: 6, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "victoria_personal@hotmail.com", number: "+55 (75) 90237-2803", sexo: "Feminino", photo: '../../../assets/Images/prof_victoria.png', name: "Victoria Dias", area: "Pilates", location: "Ferraz de Vasconcelos" },
    { id: 7, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "arthurProfessor@mail.com", number: "+55 (12) 94066-7168", sexo: "Masculino", photo: '../../../assets/Images/prof_arthur.png', name: "Arthur Fernandes", area: "Atletismo", location: "Tatuapé" },
    { id: 8, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "marcos.freitas@hotmail.com", number: "+55 (35) 98373-9418", sexo: "Masculino", photo: '../../../assets/Images/prof_marcos.png', name: "Marcos Freitas", area: "Funcional", location: "Bertioga" },
    { id: 9, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "fernando_aragao@outlook.com", number: "+55 (40) 98476-0732", sexo: "Masculino", photo: '../../../assets/Images/prof_fernando.png', name: "Fernando Aragão", area: "Judo", location: "São Mateus" },
    { id: 10, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "adolfo_alves@outlook.com", number: "+55 (72) 95573-1372", sexo: "Masculino",  photo: '../../../assets/Images/prof_adolfo.png', name: "Adolfo Alves", area: "Aero Dance", location: "Póa" },
    { id: 11, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "gustavo.stn@yandex.com", number: "+55 (58) 98969-8536", sexo: "Masculino",  photo: '../../../assets/Images/prof_gustavo.png', name: "Gustavo Santana", area: "Ginástica Rítma", location: "Alphaville" },
    { id: 12, text: "Olá me chamo Fernando, tenho 25 anos e sou  faixa preta ex judoca representante do Batalhão da Polícia, dou aulas de judô com o objetivo de repassar o conhecimento da arte suave para crianças e adultos.", email: "maria.personal@gmail.com", number: "+55 (74) 90114-0503", sexo: "Feminino",  photo: '../../../assets/Images/prof_maria.png', name: "Maria Eduarda", area: "Pedalada", location: "Morumbi" },
  ]
  public editData:any = this.dataPersonals
  public filterBy: any = null
  public open: boolean = false
  public placeholderDropDown: string = "Filtrar por modalidade"
  public modalidades = ["Mostrar tudo", "Musculação", "Natação", "Karate", "Muay Thai", "CrossFit", "Pilates", "Atletismo", "Funcional", "Judo", "Aero Dance", "Ginástica Rítma", "Pedalada"]
  public closeResult = '';

  constructor(config: NgbModalConfig, private modalService: NgbModal) { 
    config.backdrop = 'static';
    config.keyboard = true;
    config.size = "lg";
  }

  ngOnInit() { }

  openDropdown() {
    this.open = !this.open
  }

  setModalidade(modalidade: string) {
    if (modalidade == "Mostrar tudo") {
      this.placeholderDropDown = "Filtrar por modalidade"
      this.filterBy = null
    } else {
      this.filterBy = modalidade
      this.placeholderDropDown = modalidade
    }
    this.open = false
    this.showPersonalByFilter(this.filterBy)
  }

  showPersonalByFilter(param: any) {
    if (param != null) {
      this.editData = this.dataPersonals.filter(item => item.area == param)
    } else {
      this.editData = this.dataPersonals
    }
  }

  getPersonalByName(event:any){
    this.name = event.target.value
    this.editData = this.dataPersonals.filter(item => item.name.includes(this.name))
  }

  clearFilters() {
    this.name = ""
    this.showPersonalByFilter(null)
  }
  public modalInfs:any 

  openModal(content:any, data:any) {
    this.modalInfs = data
    this.modalService.open(content);
  }

  openWhatsApp(number:string){
    let formatNumber = number.replace(/\D/g, '')
    window.open(
      `https://wa.me/${formatNumber}?text=Olá Professor, vi seu anúncio na AnyPlace Fit, poderiamos conversar?`,
      '_blank'
    );
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      this.modalService.dismissAll()
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      this.modalService.dismissAll()
      return 'by clicking on a backdrop';
    } else {
      this.modalService.dismissAll()
      return `with: ${reason}`;
    }
  }
}
