import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {
  
  public imgSportsType:string = '../../assets/Images/sports_example.svg'
  public imgRunner:string = '../../assets/Images/runner.svg'
  public imgRunnerCellphone:string = '../../assets/Images/cellphone_and_runner.svg'
  public isMobile:boolean = false

  constructor() { }

  ngOnInit() { 
    this.onResize()
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.isMobile = window.innerWidth <= 450
  }

}
