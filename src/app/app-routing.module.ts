import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonalsContentComponent } from './home/personals-content/personals-content.component';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: '', component: InitialPageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'welcome', component: PersonalsContentComponent},
  {path: 'logout', component: InitialPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
   declarations: []
})
export class AppRoutingModule { }
