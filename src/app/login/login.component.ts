import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: any
  registerForm: any

  registerUser: boolean = false
  hiddenPassword: boolean = true
  hiddenPasswordRecover: boolean = true
  showMsgError: boolean = false
  disableButtomSubmit: boolean = false
  notSamePassword: boolean = false
  notSameEmail: boolean = false

  msgErrorEmail: string = ""
  msgErrorSenha: string = ""
  cpfValue: string = ""
  cepValue: string = ""
  iconPassword: string = "../../assets/Images/view.svg"
  iconPasswordRecover: string = "../../assets/Images/view.svg"
  errorSexo: boolean = false

  validFormRegister = [
    { label: 'nome', invalid: false, value: "" },
    { label: 'cpf', invalid: false, value: "" },
    { label: 'email', invalid: false, value: "" },
    { label: 'cep', invalid: false, value: "" },
    { label: 'password', invalid: false, value: "" },
    { label: 'sobrenome', invalid: false, value: "" },
    { label: 'sexo', invalid: false, value: "" },
    { label: 'confirmEmail', invalid: false, value: "" },
    { label: 'numero', invalid: false, value: "" },
    { label: 'confirmPassword', invalid: false, value: "" }
  ]

  constructor(
    private formBuilder: FormBuilder,
    private route: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      senha: [null, [Validators.required]]
    })

    this.registerForm = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      sobrenome: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      cpf: [this.cpfValue, [Validators.required, Validators.minLength(14)]],
      sexo: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      confirmEmail: [null, [Validators.required, Validators.email]],
      cep: [this.cepValue, [Validators.required, Validators.minLength(9)]],
      numero: [null, [Validators.required]],
      password: [null, [Validators.required, Validators.minLength(4)]],
      confirmPassword: [null, [Validators.required,Validators.minLength(4)]],
    })
  }

  get formLogin() { return this.loginForm.controls }
  get formRegister() { return this.registerForm.controls }

  ngOnInit() { }

  changeLogin(){
    this.registerUser = !this.registerUser
    this.disableButtomSubmit = (this.registerUser)
  }

  maskCPF(value: any) {
    value = value.target.value
    value = value.replace(/\D/g, '')
    value = value.replace(/(\d{3})(\d)/, '$1.$2') // captura 2 grupos de numero o primeiro de 3 e o segundo de 1, apos capturar o primeiro grupo ele adiciona um ponto antes do segundo grupo de numero
    value = value.replace(/(\d{3})(\d)/, '$1.$2')
    value = value.replace(/(\d{3})(\d{1,2})/, '$1-$2')
    value = value.replace(/(-\d{2})\d+?$/, '$1') // captura 2 numeros seguidos de um traço e não deixa ser digitado mais nada
    this.cpfValue = value
  }

  maskCEP(value: any) {
    value = value.target.value
    value = value.replace(/\D/g, "")
    value = value.replace(/^(\d{5})(\d)/, "$1-$2")
    this.cepValue = value
  }

  changeTypeInput(recover: boolean = false) {
    if (!recover) {
      this.hiddenPassword = !this.hiddenPassword
      this.iconPassword = (this.hiddenPassword) ? "../../assets/Images/view.svg" : "../../assets/Images/invisible.svg"
    } else {
      this.hiddenPasswordRecover = !this.hiddenPasswordRecover
      this.iconPasswordRecover = (this.hiddenPasswordRecover) ? "../../assets/Images/view.svg" : "../../assets/Images/invisible.svg"
    }
  }

  validLogin() {
    if (this.formLogin['email'].invalid) {
      this.msgErrorEmail = this.formLogin['email'].dirty ? "Informe um e-mail válido." : "Digite o seu e-mail cadastrado para logar."
      return false
    } else { this.msgErrorEmail = '' }

    if (this.formLogin['senha'].invalid) {
      this.msgErrorSenha = !this.formLogin['senha'].dirty ? "Informe uma senha para logar." : ""
      return false
    } else { this.msgErrorSenha = '' }
    return true
  }

  valid(inputName: string) {
    this.validFormRegister.map(item => {
      if (item.label == inputName) {
        item.invalid = (this.formRegister[inputName].invalid && this.formRegister[inputName].touched) ? true : false
        item.value = this.formRegister[inputName].value
      }
      return item
    })
    this.validPassword()
    this.validEmail()
    this.showMsgError = this.validFormRegister.some(valid => valid.invalid)
    if(!(this.validFormRegister.every(valid => !valid.invalid) && this.registerForm.valid)){
      this.disableButtomSubmit = false
    }
  }

  validPassword(){
    if(this.formRegister["password"].touched && this.formRegister["password"].valid && this.formRegister["confirmPassword"].touched && this.formRegister["confirmPassword"].valid){
      if(this.formRegister["password"].value != this.formRegister["confirmPassword"].value){
        this.notSamePassword = true
        this.validFormRegister[4].invalid = true
      } else {
        this.notSamePassword = false
        this.validFormRegister[4].invalid = false
      }
    }
  }

  validEmail(){
    if(this.formRegister["email"].touched && this.formRegister["email"].valid && this.formRegister["confirmEmail"].touched && this.formRegister["confirmEmail"].valid){
      if(this.formRegister["email"].value != this.formRegister["confirmEmail"].value){
        this.validFormRegister[2].invalid = true
        this.notSameEmail = true
      } else {
        this.validFormRegister[2].invalid = false
        this.notSameEmail = false
      }
    }
  }

  submit() {
    if (!this.registerUser) {
      this.validLogin() ? this.getUser() : null
    } else {
      this.saveUser()
    }
  }

  saveUser(){
    let data = this.validFormRegister.map(item => {
      return {data: item.label, value: btoa(item.value)}
    })
    localStorage.setItem("data-user", JSON.stringify(data))
    this.route.navigate(["/welcome"])
    localStorage.setItem("token", JSON.stringify("acess-true"))
  }

  loginData = [{data: '', value: ''}]

  getUser(){
    let data:any = localStorage.getItem("data-user")
    if(data != null){
      this.loginData = JSON.parse(data)
      this.loginData.map(item => {
        return {data: item.data, value: atob(item.value)}
      })    
      this.login()    
    }
    this.msgErrorEmail = "E-mail ou senha inválido."
  }

  login(){
    if(atob(this.loginData[2].value) == this.loginForm.value.email && atob(this.loginData[4].value) == this.loginForm.value.senha){
      localStorage.setItem("token", JSON.stringify("acess-true"))
      this.route.navigate(["/welcome"])
    }else{
      this.msgErrorEmail = "E-mail ou senha inválido."
    }
  }
}
