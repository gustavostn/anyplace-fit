import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { StepsRegisterComponent } from './steps-register/steps-register.component';
import { PersonalBoxComponent } from './personal-box/personal-box.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    StepsRegisterComponent,
    PersonalBoxComponent
  ],
  exports: [
    CommonModule,
    HeaderComponent,
    FooterComponent,
    StepsRegisterComponent,
    PersonalBoxComponent
  ]
})
export class SharedModule { }
