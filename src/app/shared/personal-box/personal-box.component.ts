import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'personal-box',
  templateUrl: './personal-box.component.html',
  styleUrls: ['./personal-box.component.scss']
})
export class PersonalBoxComponent implements OnInit {

  @Input() public dataPersonals = [
    {name: 'Cléo Souza', job: 'Personal', img: '../../../assets/Images/personal-fem-1.jpg'},
    {name: 'Tiago Lopes', job: 'Musculação', img: '../../../assets/Images/personal-mas-1.jpg'},
    {name: 'Isabella Fernandes', job: 'Cross Fit', img: '../../../assets/Images/personal-fem-2.jpg'},
    {name: 'Rebecca Lins', job: 'Pilates', img: '../../../assets/Images/personal-fem-3.jpg'},
    // {name: 'Cléo Souza', job: 'Funcional', img: ''},
    // {name: 'Tiago Lopes', job: 'Natação', img: ''},
    // {name: 'Isabella Fernandes', job: 'Karate', img: ''},
    // {name: 'Rebecca Lins', job: 'Pilates', img: ''},
  ]

  constructor() { }

  ngOnInit() {
  }

}
