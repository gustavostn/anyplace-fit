import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'anyplace-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  imageLogo: string = "../../../assets/Images/logo_AnyPlaceFit.svg"
  steps = [
    { id: 0, label: 'Home', route: '', current: true },
    { id: 1, label: 'Nossa historia', route: 'historia', current: false },
    { id: 2, label: 'Contato', route: 'contato', current: false },
    { id: 3, label: 'Entrar', route: '/login', current: false },
  ]

  public showDropDown: boolean = true

  constructor(private route: Router) { }

  ngOnInit() {
    this.onResize()
    this.checkRoute()
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.showDropDown = window.innerWidth >= 990
  }

  toggleDropDown() {
    this.showDropDown = !this.showDropDown
  }

  goTo = (router: string) => {
    this.route.navigate([router])
    if(router == 'logout'){
      localStorage.removeItem('token')
      localStorage.removeItem('data-user')
    }
  }

  actualPage = (setPage: number) => {
    this.steps.map(item => {
      item.current = item.id == setPage
      return item
    })
  }

  checkRoute = () => {
    if (window.location.href.includes("login")) {
      this.steps = this.steps.filter(item => item.id != 3)
    } else if (window.location.href.includes("welcome")) {
      this.steps[3].label = "Sair"
      this.steps[3].route = 'logout'
    }
  }


}
