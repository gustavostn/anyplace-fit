import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'anyplace-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  imageLogo:string = "../../../assets/Images/logo_anyplacefit_white.svg"
  iconHeart:string = "../../../assets/Images/iconHeart.svg"
  public msgFooter:string = `Feito com ${this.iconHeart} em São Paulo.`
  
  constructor() { }

  ngOnInit() {
  }

}
